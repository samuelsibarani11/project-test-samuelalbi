/* eslint-disable no-unused-vars */
import React from 'react';
import "./home.css";

const Home = () => {
    return (
        <section className="ideas-section">
            <div className="overlay">
                <h1>Ideas</h1>
                <p>Where all our great things begin</p>
            </div>
        </section>
    );
};

export default Home;
