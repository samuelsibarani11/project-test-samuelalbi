/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import './header.css';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

const Header = () => {
    const [prevScrollPos, setPrevScrollPos] = useState(window.pageYOffset);
    const [visible, setVisible] = useState(true);

    useEffect(() => {
        const handleScroll = () => {
            const currentScrollPos = window.pageYOffset;
            setVisible(prevScrollPos > currentScrollPos || currentScrollPos < 10);
            setPrevScrollPos(currentScrollPos);
        };

        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, [prevScrollPos]);

    return (
        <div className={`fixed-top ${visible ? 'active' : 'hidden'}`}>
            <Navbar expand="lg" className="navbar_container">
                <Container>
                    <Navbar.Brand href="#home"><img src="/images/suitmedia-logo.png" alt="" width={150} /></Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="ms-auto gap-link">
                            <Nav.Link href="#home" className='text-white'>Home</Nav.Link>
                            <Nav.Link href="#about" className='text-white'>About</Nav.Link>
                            <Nav.Link href="#services" className='text-white'>Services</Nav.Link>
                            <Nav.Link href="#ideas" className='text-white active-link'>Ideas</Nav.Link>
                            <Nav.Link href="#careers" className='text-white'>Careers</Nav.Link>
                            <Nav.Link href="#contact" className='text-white'>Contact</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    );
};

export default Header;
