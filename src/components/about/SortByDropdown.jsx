/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import "./page.css"

const SortByDropdown = ({ options, defaultOption, onOptionChange }) => {
  const [selectedOption, setSelectedOption] = useState(defaultOption);

  const handleChange = (event) => {
    const value = event.target.value;
    setSelectedOption(value);
    onOptionChange(value);
  };

  return (
    <div className="dropdown-container">
      <label htmlFor="show-per-page" className="dropdown-label">Show per page:</label>
      <select
        id="show-per-page"
        value={selectedOption}
        onChange={handleChange}
        className="dropdown-select"
      >
        {options.map((option, index) => (
          <option key={index} value={option}>
            {option}
          </option>
        ))}
      </select>
    </div>
  );
};

export default SortByDropdown;
