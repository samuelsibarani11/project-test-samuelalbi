/* eslint-disable react/prop-types */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './about.css';
import Card from 'react-bootstrap/Card';
import ShowPerPageDropdown from './ShowPerPageDropdown';
import SortByDropdown from './SortByDropdown';

const About = () => {
    const [perPage, setPerPage] = useState(10);
    const [sortBy, setSortBy] = useState('-published_at');
    const [currentPage, setCurrentPage] = useState(1);
    const [items, setItems] = useState([]);
    const [totalItems, setTotalItems] = useState(0);

    useEffect(() => {
        fetchItems();
    }, [perPage, sortBy, currentPage]);

    const fetchItems = async () => {
        const url = `https://suitmedia-backend.suitdev.com/api/ideas?page[number]=${currentPage}&page[size]=${perPage}&append[]=small_image&append[]=medium_image&sort=${sortBy}`;
        try {
            const response = await axios.get(url, {
                proxy: {
                    host: 'localhost',
                    port: 3306
                },
                headers: {
                    'Content-Type': 'application/json',
                },
            });
            if (response.status !== 200) {
                throw new Error('Failed to fetch data');
            }
            const data = response.data;
            console.log('Fetched data:', data);
            setItems(data.data);
            setTotalItems(data.meta.total);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    const handlePerPageChange = (option) => {
        setPerPage(option);
        setCurrentPage(1);
    };

    const handleSortByChange = (option) => {
        setSortBy(option);
    };

    const handlePageChange = (event, page) => {
        event.preventDefault();
        setCurrentPage(page);
    };

    const Pagination = ({ totalItems, perPage, currentPage, handlePageChange }) => {
        const totalPages = Math.ceil(totalItems / perPage);
        const maxPagesToShow = 10;
        let startPage, endPage;

        if (totalPages <= maxPagesToShow) {
            // Show all pages
            startPage = 1;
            endPage = totalPages;
        } else {
            // Show partial pages with ellipses
            if (currentPage <= 6) {
                // Current page near the start
                startPage = 1;
                endPage = maxPagesToShow;
            } else if (currentPage + 4 >= totalPages) {
                // Current page near the end
                startPage = totalPages - maxPagesToShow + 1;
                endPage = totalPages;
            } else {
                // Current page somewhere in the middle
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }

        const pages = [...Array((endPage + 1) - startPage).keys()].map(i => startPage + i);

        return (
            <div className="pagination">
                <a href="#" onClick={(event) => handlePageChange(event, 1)}>&laquo;</a>
                <a href="#" className="arrow" onClick={(event) => handlePageChange(event, Math.max(currentPage - 1, 1))}>&#8249;</a>
                {startPage > 1 && <span>...</span>}
                {pages.map(page => (
                    <a href="#" key={page} className={currentPage === page ? "active" : ""} onClick={(event) => handlePageChange(event, page)}>{page}</a>
                ))}
                {endPage < totalPages && <span>...</span>}
                <a href="#" className="arrow" onClick={(event) => handlePageChange(event, Math.min(currentPage + 1, totalPages))}>&#8250;</a>
                <a href="#" onClick={(event) => handlePageChange(event, totalPages)}>&raquo;</a>
            </div>
        );
    };

    return (
        <section className='about-margin'>
            <div className='container'>
                {/* Show Section */}
                <div className='d-flex flex-column flex-md-row justify-content-between align-items-start align-items-md-center my-3'>
                    <p className='mb-2 mb-md-0'>Showing {(currentPage - 1) * perPage + 1} - {Math.min(currentPage * perPage, totalItems)} of {totalItems}</p>
                    <div className='d-flex flex-column flex-md-row'>
                        <div className='mb-2 mb-md-0 mx-md-3'>
                            <ShowPerPageDropdown
                                options={[10, 20, 50]}
                                defaultOption={10}
                                onOptionChange={handlePerPageChange}
                            />
                        </div>
                        <div>
                            <SortByDropdown
                                options={['-published_at', 'published_at']}
                                defaultOption={'-published_at'}
                                onOptionChange={handleSortByChange}
                            />
                        </div>
                    </div>
                </div>

                {/* Card Section */}
                <div className='row'>
                    {items.map(item => (
                        <div className='col-12 col-md-6 col-lg-3 mb-4' key={item.id}>
                            <Card className="h-100 no-padding">
                                {item.medium_image && item.medium_image[0] && item.medium_image[0].url ? (
                                    <Card.Img variant="top" src={item.medium_image[0].url} className="card-img-top-custom no-padding" loading="lazy" />
                                ) : (
                                    <Card.Img variant="top" src="placeholder-image-url.jpg" className="card-img-top-custom no-padding" loading="lazy" />
                                )}
                                <Card.Body>
                                    <Card.Text className="text-muted small">{new Date(item.published_at).toDateString()}</Card.Text>
                                    <Card.Title className="card-title-custom">{item.title}</Card.Title>
                                </Card.Body>
                            </Card>
                        </div>
                    ))}
                </div>

                {/* Pagination Section */}
                <Pagination
                    totalItems={totalItems}
                    perPage={perPage}
                    currentPage={currentPage}
                    handlePageChange={handlePageChange}
                />
            </div>
        </section>
    );
};

export default About;
